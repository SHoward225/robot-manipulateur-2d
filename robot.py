#--------Importation des librairies Python

import tkinter as tk 
from tkinter import *
from PIL import Image,ImageTk
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import math
import numpy as np
import time
import connexion

def principale():

    ##################################################################################################################
    #                          Definition des fonctions de notre application
    ##################################################################################################################


    #Fonctionnalité de calcul de l'inverse ==> MGI

    def inverse(x,y):
        # Cette fonction implémente le modèle géométrique inverse

        L0 = recupValeurLien(saisie_lien0)
        L1 = recupValeurLien(saisie_lien1)
        L2 = recupValeurLien(saisie_lien2)
        B1 = -2*y*L1
        B2 = 2*L1*(L0-x)
        B3 = L2**2-y**2-(L0-x)**2-L1**2
        teta_1=0
        teta_2=0
        SO1 = 0
        CO1 = 0
        epsi = 1
        if B3==0 :
            teta_1 = math.degrees(math.atan2(-B2,B1))
        else:
            if ((B1**2+B2**2-B3**2)>=0) :
                SO1 = (B3*B1+epsi*B2*math.sqrt(B1**2+B2**2-B3**2))/(B1**2+B2**2)
                CO1 = (B3*B2-epsi*B1*math.sqrt(B1**2+B2**2-B3**2))/(B1**2+B2**2)
                teta_1 = math.degrees(math.atan2(SO1,CO1))
            
        Yn1 = L2*SO1
        Yn2 = L2*CO1
        if L2!=0 :
            teta_2 = math.degrees(math.atan2(Yn1/L2,Yn2/L2))

        return [teta_1,teta_2]



    def verifSaisieInt(valeur):
        try:
            f =int(valeur)
            return True
        except:
            return False



    def verifSaisieFloat(valeur):
        try:
            f =float(valeur)
            return True
        except:
            return False



    def recupValeurLien(case):
        if (verifSaisieFloat(case.get())==True):
            return float(case.get())
        else:
            case.delete(0,END)
            case.insert(0,"3")
            return 3



    def recupValeurAngle(angle):
        if (verifSaisieFloat(angle.get())==True):
            return float(angle.get())
        else:
            angle.delete(0,END)
            angle.insert(0,"30")
            return 30


    def recupValeur(txt):
        if (verifSaisieInt(txt.get())==True):
            return int(txt.get())
        else:
            txt.delete(0,END)
            txt.insert(0,"2")
            return 2


    # Fonction de calcul du MGD
    def calcul():
        
        L0 = recupValeurLien(saisie_lien0)
        L1 = recupValeurLien(saisie_lien1)
        L2 = recupValeurLien(saisie_lien2)
        O1 = math.radians(recupValeurAngle(saisie_angle1))
        O2 = math.radians(recupValeurAngle(saisie_angle2))
        nbrePas = int(recupValeur(saisie_NbPas))
        YB = recupValeurLien(saisie_ordonneeB)
        XB = recupValeurLien(saisie_abscisseB)
        temps=recupValeur(saisie_duree)
        #LES MATRICES DE PASSAGE DIRECTE
        Mat0T1 = np.array([[math.cos(O1),-math.sin(O1),0,L0],[math.sin(O1),math.cos(O1),0,0],[0,0,1,0],[0,0,0,1]], dtype=object)
        Mat1T2 = np.array([[math.cos(O2),-math.sin(O2),0,L1],[math.sin(O2),math.cos(O2),0,0],[0,0,1,0],[0,0,0,1]], dtype=object)
        Mat0T2 = Mat0T1.dot(Mat1T2)
        A2=np.array([[L2],[0],[0],[1]], dtype=object) 
        A21=np.array([[L1],[0],[0],[1]], dtype=object) 
        A10=np.array([[L0],[0],[0],[1]], dtype=object) 
        A0 = Mat0T2.dot(A2)
        #Position de A2 dans le R0
        A20 = Mat0T1.dot(A21)
        #Angle de rotation de 0A2
        rotAngle = math.degrees(math.atan2(Mat0T2[[1],[0]], Mat0T2[[0],[0]]))
        #LES MATRICES DE PASSAGE INVERSE
        Mat1T0 = np.array([[math.cos(O1),math.sin(O1),0,-L0*math.cos(O1)],[-math.sin(O1),math.cos(O1),0,L0*math.sin(O1)],[0,0,1,0],[0,0,0,1]], dtype=object)
        Mat2T1 = np.array([[math.cos(O2),math.sin(O2),0,-L0*math.cos(O2)],[-math.sin(O2),math.cos(O2),0,L1*math.sin(O2)],[0,0,1,0],[0,0,0,1]], dtype=object)
        #txtXA.insert(0,float(A0[1]))
        #txtYA.insert(0,float(A0[0]))
        return [ L0, A20, A0, L1, L2,nbrePas,YB,XB,temps]



    #Fonctionnalité du bouton dessiner

    def dessiner():
        plot.cla()
        result = calcul()
        L0 = result[0]
        A20 = result[1]
        A0 = result[2]
        #YB = result[4]
        #XB = result[5]
        #nbreMaxPas = result[0]
        plot.set_xlabel('Axe Y0')
        plot.set_ylabel('Axe X0')
        plot.yaxis.set_ticks_position('right')
        plot.set_xticks(range(10))
        plot.set_yticks(range(10))
        plot.set_xlim((9,0))
        plot.set_ylim((0, 9))
        plot.grid(True)

        #tracer L0
        plot.plot([0.5,0.5],[0.0,L0],"b-",lw=5)
        #tracer L1
        plot.plot([0.5,A20[1]],[L0,A20[0]],"b-",lw=5)
        #tracer L2
        plot.plot([A20[1],A0[1]],[A20[0],A0[0]],"b-",lw=5)
        #Le point A
        plot.scatter([A0[1]], [A0[0]], s =500, color = 'red')
        #Le point B
        #plot.scatter([YB], [XB], s =500, color = 'red')
        #Les Articulations
        plot.scatter([0.5], [L0], s =500, color = 'orange')
        plot.scatter([A20[1]], [A20[0]], s =500, color = 'orange')
        #La base et le sol
        plot.plot([0.5,0.7],[0.0,0.0],"k-",lw=10)
        plot.plot([0.0,15.0],[0.0,0.0],"k--",lw=3)
        graphique.draw()



    #Fonctionnalité du bouton simuler

    def simuler():
        etat_simuler=True
        global X_Pi, Y_Pi
        X_Pi = []
        Y_Pi = []
        result = calcul()
        nbrePas = result[5]
        L0 = result[0]
        L1 = result[3]
        L2 = result[4]
        YB = result[6]
        A0 = result[2]
        XB = result[7]
        A20 = result[1]
        temps=result[8]
        vitesse = float(temps/nbrePas)
        
        for i in range(1,nbrePas+1):

            # On efface le graphe dessiné dans le cavas puis 
            # on redessine le nouveau graphe avec les mêmes 
            # caracteristiques.

            plot.cla()
            
            plot.set_xlabel('Axe Y0')
            plot.set_ylabel('Axe X0')
            plot.yaxis.set_ticks_position('right')
            plot.set_xticks(range(10))
            plot.set_yticks(range(10))
            plot.set_xlim((9,0))
            plot.set_ylim((0,9))
            plot.grid(True)
            
            # On détermine les coordonnées du point Pk

            #Distance dans la direction X
            disXPas = (XB-A0[0])/nbrePas
            if disXPas<0:
                disXPas = -disXPas

            #Distance dans la direction Y
            disYPas = (YB-A0[1])/nbrePas
            if disYPas<0:
                disYPas = -disYPas
            
            # Coordonnées du point Pk
            if XB>=A0[0] :
                Xi = A0[0]+i*disXPas
            else:
                Xi = A0[0]-i*disXPas

            if YB>A0[1]:
                Yi = A0[1]+i*disYPas
            else:
                Yi = A0[1]-i*disYPas

            # Détermination des variables articulaires théta 1 et théta 2

            theta=inverse(Xi,Yi)
            
            # Coordonnées du point A2

            XA2i =L1*math.cos(math.radians(theta[0]))+L0
            YA2i =L1*math.sin(math.radians(theta[0]))
            
            #Trajectoire
            
            XA0 = result[2][0]
            XB = result[7]
            YA0 = result[2][1]
            YB = result[6]
            a = (YA0-YB)/(XA0-XB)
            b = YB-a*XB
            x=range(-100,101)
            y = a*x + b
            #Trace la droite
            plot.plot([YB,YA0],[XB,XA0],lw=5)
            
            #Droite entre A et Pi
            plot.plot([A0[1],Yi],[A0[0],Xi],"y-",lw=5)
            #sauvegarde les coordonnees des Pi
            X_Pi.append(Xi)
            Y_Pi.append(Yi)
            
            #Les Pas
            
            for j in range(0,len(X_Pi)) :
                plot.scatter([Y_Pi[j]], [X_Pi[j]], color = '#FF00CC')

            #tracer L0
            plot.plot([0.5,0.5],[0.0,L0],"b-",lw=7)
            #tracer L1
            plot.plot([0.5,YA2i],[L0,XA2i],"b-",lw=7)
            #tracer L2
            plot.plot([YA2i,Yi],[XA2i,Xi],"b-",lw=7)
            #Point Pi
            plot.scatter([Yi], [Xi], color = '#FF0000')
            #Point A0
            plot.scatter([0.5], [L0], s =500, color = 'black')
            #Point A2
            plot.scatter([YA2i], [XA2i], s =500, color = 'black')
            if i!=0:
                #Le point A
                plot.scatter([A0[1]], [A0[0]], s =100, color = '#006633')
            else:
                #Le point A
                plot.scatter([A0[1]], [A0[0]], s =500, color = '#FF0000')
                
            if i==nbrePas:
                #Le point B
                plot.scatter([YB], [XB], s =300, color = '#FF0000')
            else:
                #Le point B
                plot.scatter([YB], [XB], s =300, color = '#00FF33')
            
            #Le sol
            plot.plot([0.5,0.7],[0.0,0.0],"k-",lw=10)
            plot.plot([0.0,15.0],[0.0,0.0],"k--",lw=3)
            
            graphique.draw()

            time.sleep(vitesse)
            root.update()
            if(i == 1):
                var.insert(0,"Coordonnées du point A \n" )
                var.insert(1,'('+str(XA0)+','+str(YA0)+')')
                var.insert(2,"Coordonnées des points Pk \n" )
            var.insert(3,'('+str(X_Pi[i-1])+','+str(Y_Pi[i-1])+')')


    def logOut():
        root.destroy()
        connexion.connexion()


    #Fonctionnalité du bouton demo
    def demo():
        result =[3.5,3,3,55,75,0,1,10,5]
        nbrePas = result[7]
        L0 = result[0]
        L1 = result[1]
        L2 = result[2]
        YB = result[6]
        XB = result[5]
        O1=result[3]
        O2=result[4]
        temps=result[8]
        saisie_lien0.delete(0,END)
        saisie_lien0.insert(0,L0)
        saisie_lien1.delete(0,END)
        saisie_lien1.insert(0,L1)
        saisie_lien2.delete(0,END)
        saisie_lien2.insert(0,L2)
        saisie_angle1.delete(0,END)
        saisie_angle1.insert(0,O1)
        saisie_angle2.delete(0,END)
        saisie_angle2.insert(0,O2)
        saisie_abscisseB.delete(0,END)
        saisie_abscisseB.insert(0,XB)
        saisie_ordonneeB.delete(0,END)
        saisie_ordonneeB.insert(0,YB)
        saisie_NbPas.delete(0,END)
        saisie_NbPas.insert(0,nbrePas)
        saisie_duree.delete(0,END)
        saisie_duree.insert(0,temps)


    #Fonctionnalité du bouton nouveau
    def nouveau():
        saisie_lien0.delete(0,END)
        saisie_lien1.delete(0,END)
        saisie_lien2.delete(0,END)
        saisie_angle1.delete(0,END)
        saisie_angle2.delete(0,END)
        saisie_abscisseB.delete(0,END)
        saisie_ordonneeB.delete(0,END)
        saisie_NbPas.delete(0,END)
        saisie_duree.delete(0,END)

        plot.cla()
        plot.set_xlabel('Axe Y0')
        plot.set_ylabel('Axe X0')
        plot.yaxis.set_ticks_position('right')
        plot.set_xticks(range(10))
        plot.set_yticks(range(10))
        plot.set_xlim((9,0))
        plot.set_ylim((0,9))
        plot.grid(True)
        graphique.draw()

        frame_infos = Frame(root, width=350, height=170, bg=background, border=2, relief="groove")
        label_infos = Label(frame_infos,text="Informations de débogage",justify="left",font="AgencyFB 8 bold",bg="grey",fg="black")
        label_infos.place(x=0,y=0, width=350)

    

    #########################################################################################
    #                          Fenêtre principale de notre application
    ########################################################################################


    colorBase="#F16824" #orange
    background="#E2F0D9" #jaune-vert

    root = Tk()
    root.title("Robot Manipulateur 2D")
    root.geometry("725x620+400+50")
    root.configure(background=colorBase)
    root.resizable(width=False, height=False)

    #Les titres
    rootTitre = Label(root, border=5, relief=RIDGE, text="ROBOT MANIPULATEUR 2D", font=("Arial", 30), background= colorBase, foreground="white")
    rootTitre.place(x=0, y=0, width=725)

    #########################################################################################
    #                           Creation des widgets 
    ########################################################################################

    #---------------------------------- Paramètres --------------------------------------------
    frame_parametre = Frame(root, width=170, height=530, bg=background, border=2, relief="groove")
    frame_parametre.place(x=10, y=70)

    #Interface graphique des parametres
    label_parametres = Label(frame_parametre,text="LES PARAMETRES",anchor= 'w',font="AgencyFB 12 bold underline",bg=background,fg="black")
    label_parametres.place(x=0,y=25, width=160)

    label_liens = Label(frame_parametre,text="Variables géométriques",anchor= 'w',font="AgencyFB 8 bold",bg="grey",fg="black")
    label_liens.place(x=0,y=70, width=160)

    label_lien0 = Label(frame_parametre,text="Longueur du lien L0",anchor= 'w',font="AgencyFB 8 bold",bg="#E2F0D9",fg="black")
    label_lien0.place(x=2,y=100, width=120)

    saisie_lien0 = Entry(frame_parametre,bd=1, width=6,font="AgencyFB 8 bold",bg="white", selectbackground="blue",fg="black",justify="left")
    saisie_lien0.place(x=120, y=100)

    label_lien1 = Label(frame_parametre,text="Longueur du lien L1",anchor= 'w',font="AgencyFB 8 bold",bg="#E2F0D9",fg="black")
    label_lien1.place(x=2,y=130, width=120)

    saisie_lien1 =  Entry(frame_parametre,bd=1, width=6,font="AgencyFB 8 bold",bg="white", selectbackground="blue",fg="black",justify="left")
    saisie_lien1.place(x=120, y=130)

    label_lien2 = Label(frame_parametre,text="Longueur du lien L2",anchor= 'w',font="AgencyFB 8 bold",bg="#E2F0D9",fg="black")
    label_lien2.place(x=2,y=160, width=120)

    saisie_lien2 =  Entry(frame_parametre,bd=1, width=6,font="AgencyFB 8 bold",bg="white", selectbackground="blue",fg="black",justify="left")
    saisie_lien2.place(x=120, y=160)

    label_angles = Label(frame_parametre,text="Variables articulaires",anchor= 'w',font="AgencyFB 8 bold",bg="grey",fg="black")
    label_angles.place(x=0,y=200, width=160)

    label_angle1 = Label(frame_parametre,text="Mesure de l'angle θ1",anchor= 'w',font="AgencyFB 8 bold",bg="#E2F0D9",fg="black")
    label_angle1.place(x=2,y=230, width=120)

    saisie_angle1 =  Entry(frame_parametre,bd=1, width=6,font="AgencyFB 8 bold",bg="white", selectbackground="blue",fg="black",justify="left")
    saisie_angle1.place(x=120, y=230)

    label_angle2 = Label(frame_parametre,text="Mesure de l'angle θ2",anchor= 'w',font="AgencyFB 8 bold",bg="#E2F0D9",fg="black")
    label_angle2.place(x=2,y=260, width=120)

    saisie_angle2 = Entry(frame_parametre,bd=1, width=6,font="AgencyFB 8 bold",bg="white", selectbackground="blue",fg="black",justify="left")
    saisie_angle2.place(x=120, y=260)

    label_pointB = Label(frame_parametre,text="Point B à atteindre",anchor= 'w',font="AgencyFB 8 bold",bg="grey",fg="black")
    label_pointB.place(x=0,y=300, width=160)

    label_abscisseB = Label(frame_parametre,text="Abscisse du point B",anchor= 'w',font="AgencyFB 8 bold",bg="#E2F0D9",fg="black")
    label_abscisseB.place(x=2,y=330, width=120)

    saisie_abscisseB = Entry(frame_parametre,bd=1, width=6,font="AgencyFB 8 bold",bg="white", selectbackground="blue",fg="black",justify="left")
    saisie_abscisseB.place(x=120, y=330)

    label_ordonneeB = Label(frame_parametre,text="Ordonnée du point B",anchor= 'w',font="AgencyFB 8 bold",bg="#E2F0D9",fg="black")
    label_ordonneeB.place(x=2,y=360, width=120)

    saisie_ordonneeB = Entry(frame_parametre,bd=1, width=6,font="AgencyFB 8 bold",bg="white", selectbackground="blue",fg="black",justify="left")
    saisie_ordonneeB.place(x=120, y=360)

    label_autres = Label(frame_parametre,text="Paramètres du mouvement",anchor= 'w',font="AgencyFB 8 bold",bg="grey",fg="black")
    label_autres.place(x=0,y=400, width=160)

    label_NbPas = Label(frame_parametre,text="Nombre de pas ",anchor= 'w',font="AgencyFB 8 bold",bg="#E2F0D9",fg="black")
    label_NbPas.place(x=2,y=430, width=120)

    saisie_NbPas =  Entry(frame_parametre,bd=1, width=6,font="AgencyFB 8 bold",bg="white", selectbackground="blue",fg="black",justify="left")
    saisie_NbPas.place(x=120, y=430)

    label_duree = Label(frame_parametre,text="Durée du trajet A-B ",anchor= 'w',font="AgencyFB 8 bold",bg="#E2F0D9",fg="black")
    label_duree.place(x=2,y=460, width=120)

    saisie_duree =  Entry(frame_parametre,bd=1, width=6,font="AgencyFB 8 bold",bg="white", selectbackground="blue",fg="black",justify="left")
    saisie_duree.place(x=120, y=460)

    # Titre Bouton
    bouton_deconnecter= Button(frame_parametre,text="Log out",font="AgencyFB 10 bold",justify="center",bg="blue",fg="white",width=6,height=1,bd=5, command=logOut)
    bouton_deconnecter.place(x=40, y=490)


    #------------------------ Dessin ----------------------------
    frame_dessin = Frame(root, width=350, height=345, bg=background, border=2, relief="groove")
    frame_dessin.place(x=190, y=70)

    # Creation du cadre et du repère
    largeur=320
    hauteur=320
    graphe=Canvas(frame_dessin,width=largeur,height=hauteur)
    graphe.place(x=10, y= 5) #Positionnement du cadre du  graphe
    schema = Figure(figsize=(6, 6), dpi=52)
    plot = schema.add_subplot(1, 1, 1)
    plot.set_xlabel('Axe Y0')
    plot.set_ylabel('Axe X0')
    plot.yaxis.set_ticks_position('right')
    plot.set_xticks(range(11))
    plot.set_yticks(range(11))
    plot.set_xlim((11,0))
    plot.set_ylim((0, 11))
    plot.grid(True)

    graphique = FigureCanvasTkAgg(schema, graphe)
    graphique.draw()
    graphique.get_tk_widget().place(x=5, y= 5) #Positionnement du graphe

    #------------------------- Information
    frame_infos = Frame(root, width=350, height=170, bg=background, border=2, relief="groove")
    frame_infos.place(x=190, y=425)

    # Titre
    label_infos = Label(frame_infos,text="Informations de débogage",width=48,justify="left",font="AgencyFB 8 bold",bg="grey",fg="black")
    label_infos.place(x=0, y=0)

    #Scrollbar
    scroll= Scrollbar(frame_infos)
    scroll.pack(side=RIGHT,fill=Y)
    var=Listbox(frame_infos,yscrollcommand=scroll.set,width=54,height=8,justify="center")
    for i in range(200):
        var.insert(END,"")
    var.pack(pady = 20,side=LEFT,fill=BOTH)
    #var.config(width=100)
    #var.place(x=0,y=20)
    scroll.config(command=var.yview)



    #---------------------------------- Boutons --------------------------------------------

    #Frame Bouton
    frame_bouton = Frame(root, width=165, height=345, bg=background, border=2, relief="groove")
    frame_bouton.place(x=550, y=70)

    # Titre Bouton
    label_bouton = Label(frame_bouton,text="Boutons du simulateur",justify="left",font="AgencyFB 8 bold",bg="grey",fg="black")
    label_bouton.place(x=0,y=0, width=165)

    # Bouton demo
    bouton_demo= Button(frame_bouton,text="Demo",font="AgencyFB 10 bold",justify="center",bg="orange",fg="white",width=8,height=1,bd=5, command=demo)
    bouton_demo.place(x=30, y=40)

    # Bouton dessiner
    bouton_dessiner= Button(frame_bouton,text="Dessiner",font="AgencyFB 10 bold",justify="center",bg="#70AD47",fg="white",width=8,height=1,bd=5, command=dessiner)
    bouton_dessiner.place(x=30, y=100)

    # Bouton simuler
    bouton_simuler= Button(frame_bouton,text="Simuler",font="AgencyFB 10 bold",justify="center",bg="#70AD47",fg="white",width=8,height=1,bd=5, command=simuler)
    bouton_simuler.place(x=30, y=160)

    # Bouton nouveau
    bouton_nouveau= Button(frame_bouton,text="Nouveau",font="AgencyFB 10 bold",justify="center",bg="grey",fg="white",width=8,height=1,bd=5, command=nouveau)
    bouton_nouveau.place(x=30, y=220)

    # Bouton quitter
    bouton_quitter= Button(frame_bouton,text="Quitter",font="AgencyFB 10 bold",justify="center",bg="red",fg="white",width=8,height=1,bd=5, command=root.destroy)
    bouton_quitter.place(x=30, y=280)

    #------------------------------------- AUTEUR ----------------------------------------------

    #Frame
    frame_auteur = Frame(root, width=165, height=175, bg=background, border=2, relief="groove")
    frame_auteur.place(x=550, y=425)

    # Titre Auteur
    label_auteur = Label(frame_auteur,text="Auteur du simulateur",justify="left",font="AgencyFB 8 bold",bg="grey",fg="black")
    label_auteur.place(x=0,y=0, width=165)

    # Image de l'auteur
    canvas = Canvas(frame_auteur, width = 120, height = 150)  
    canvas.place(x=20, y=20, width=130, height=110)
    img = ImageTk.PhotoImage(Image.open("Stephane.png"))  
    canvas.create_image(65, 60, anchor="center", image=img)

    # Nom de l'auteur
    nom_auteur = Label(frame_auteur,bg="#808080", text='KOUADIO Stephane', font="AgencyFB 10 bold")
    nom_auteur.place(x=20, y=140)


    root.mainloop()

if __name__=="__main__":
    principale()
