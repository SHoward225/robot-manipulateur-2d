# -*- coding: utf-8 -*-
"""
Created on Thu Nov  1 00:59:00 2022

@author: HOWARD
"""

from tkinter import * #Bibliotheque principale
from PIL import ImageTk,Image #importation d'images
import sqlite3 #Gestion de la base de donnee
from tkinter.messagebox import showerror #affichage de message d'alerte
from tkinter import messagebox

import connexion
        
######################################################################################################
#                               Creation de la fenetre de simulation 
######################################################################################################

def inscription():

    background="#E2F0D9" #bleu

    bg_color = "#758CB0" #"DeepSkyBlue2"
    fg_color = "#383a39"

    root = Tk()
    root.geometry("600x600+500+100")
    root.title(" Page d'inscription ")
    root.configure(background= "#F16824")
    root.resizable(width=False, height=False)

    

    log_frame= Frame(root, width=500, height=500, bg=background, border=2, relief="groove")
    log_frame.place(x=50, y=50)

    data = "base_donnees/Robot.db"

    #---heading image
    photo = ImageTk.PhotoImage(Image.open("RT.jpeg"))
    image = Label(log_frame, image=photo)
    image.place(x=0, y=0)

    rootTitre = Label(log_frame, border=2, relief=RIDGE, text="Bienvenu sur notre page de connexion", font= "Arial 15 bold", background= "#758CB0", foreground="white")
    rootTitre.place(x=0, y=0, width=500)


    # -------Label et champs username
    label_username = Label(log_frame,border=2, text="Username :", fg="white", bg=bg_color, font= "Arial 10 bold")
    label_username.place(x=120, y=100)

    saisie_username = Entry(log_frame, border=3, width=25)
    saisie_username.place(x=230, y=100)

    # -------Label et champs nom complet
    label_nom = Label(log_frame,border=2, text="Nom Complet :", fg="white", bg=bg_color, font= "Arial 10 bold")
    label_nom.place(x=120, y=150)

    saisie_nom = Entry(log_frame, border=3, width=25)
    saisie_nom.place(x=230, y=150)

    # -------Label et champs Ville/Commune
    label_city = Label(log_frame,border=2, text="Ville/Commune :", fg="white", bg=bg_color, font= "Arial 10 bold")
    label_city.place(x=120, y=200)

    saisie_city= Entry(log_frame, border=3, width=25)
    saisie_city.place(x=230, y=200)

    # ----Label et champ password
    label_password = Label(log_frame,  text="Password:", fg="white", bg=bg_color, font= "Arial 10 bold")
    label_password.place(x=120, y=250)
    saisie_password = Entry(log_frame, show="*", border=3, width=25)
    saisie_password.place(x=230, y=250)

    # ----Label et champ valider password
    label_valider_password = Label(log_frame,  text="Valider Password :", fg="white", bg=bg_color, font= "Arial 10 bold")
    label_valider_password.place(x=120, y=300)
    saisie_valider_password = Entry(log_frame, show="*", border=3, width=25)
    saisie_valider_password.place(x=230, y=300)

    def checkAuthentification():
        root.destroy()
        connexion.connexion()


    def connexionDB():
        global cursor
      

    #Fonction d'authentification par rapport a notre base de donnée
    def authentification():
        global cursor
        data = "base_donnees/Robot.db"
        login = saisie_username.get() + ''
        pwd1 = saisie_password.get() + ''
        name = saisie_nom.get() + ''
        city = saisie_city.get() + ''
        pwd2 = saisie_valider_password.get() + ''

        if pwd1 != pwd2:
            info = messagebox.showinfo('Information', "Vous avez entrer des mots de passes differents", parent=root)

        elif (login == "" or pwd1=="" or name== "" or city == "" or pwd2=="") :
            info = messagebox.showinfo('Information', "Vueillez renseigner tous champs SVP", parent=root)
        else:
            connection=sqlite3.connect(data)
            try:
                
                cursor = connection.cursor() #creation d'une instance de cursor

                cursor.execute(f"INSERT INTO t_users(usr_name,usr_password, name, usr_city) VALUES ('{login}', '{pwd1}', '{name}', '{city}')")
                connection.commit()
                checkAuthentification()

            except Exception as e:
                print("Error : ", e)
                info = messagebox.showinfo('Information', " Veuillez renseigner tous les champs", parent=root)
                connection.rollback()
            finally:
                connection.close

    connexionDB()

    #Bouton de validation
    btn_valider = Button(log_frame, text="Login",borderwidth=3, relief='ridge', fg="white", bg=bg_color, width = 15, command=authentification)
    btn_valider.place(x=200, y=400)

    root.mainloop()

if __name__=="__main__":
    inscription()
