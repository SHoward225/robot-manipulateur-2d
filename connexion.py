# -*- coding: utf-8 -*-
"""
Created on Thu Nov  1 00:59:00 2022

@author: KOUADIO Konan Junior Aimé Stephane
"""

from tkinter import * #Bibliotheque principale
from PIL import ImageTk,Image #importation d'images
import sqlite3 #Gestion de la base de donnee
from tkinter.messagebox import showerror #affichage de message d'alerte
from tkinter import messagebox

import robot, inscription

def connexion():
        

    #------------------------------------- Creation de la fenetre de simulation -----------------------------------
    root = Tk()
    background="#E2F0D9" #bleu

    bg_color = "#758CB0"
    fg_color = "#383a39"

    root.geometry("500x500+500+150")
    root.title("Page d'authentification")
    root.configure(background= "#F16824")
    root.resizable(width=False, height=False)


    data = "base_donnees/Robot.db"
    cursor = None
    
    #Fonction de connexion a notre base de donnée
    def connexionDB():
        global cursor, data
        try:
            connexion = sqlite3.connect(data)
            cursor = connexion.cursor()

        except Exception as e:
            print("[ERREUR] ", e)


    #Fonction d'authentification par rapport a notre base de donnée
    def authentification():
        
        login = saisie_username.get() + ''
        pwd = saisie_password.get() + ''

        utilisateur = (login, pwd)

        try:
            connection = sqlite3.connect(data)
            cursor = connection.cursor()

            req = cursor.execute('SELECT * FROM t_users WHERE usr_name = ? and usr_password = ?', utilisateur)

            if req.fetchall():
                checkAuthentification()

            else:
                info = messagebox.showinfo('Information', "Nom d'utlisateur ou Mot de passe incorrect", parent=root)
        except Exception as e:
            print("[ERREUR] ", e)
        finally:
            connection.close


    connexionDB()


    log_frame= Frame(root, width=320, height=350, bg=background, border=2, relief="groove")
    log_frame.place(x=90, y=60)


    label_log_frame = Label(log_frame,text="CONNEXION",anchor= 'w',font="AgencyFB 12 bold underline",bg=background, fg="white")
    label_log_frame.place(x=0,y=25, width=160)


    #---heading image
    photo = ImageTk.PhotoImage(Image.open("robotic-arm.jpg"))
    image = Label(log_frame, image=photo)
    image.place(x=0, y=30)

    rootTitre = Label(log_frame, border=2, relief=RIDGE, text="Connexion", font= "Arial 30 bold", background= "#758CB0", foreground="white")
    rootTitre.place(x=0, y=0, width=317)


    # -------Label et champs username
    label_username = Label(log_frame,border=2, text="Username:", fg="white", bg=bg_color, font= "Arial 10 bold")
    label_username.place(x=50, y=100)

    saisie_username = Entry(log_frame, border=3)
    saisie_username.place(x=150, y=100)

    # ----Label et champ password
    label_password = Label(log_frame,  text="Password:", fg="white", bg=bg_color, font= "Arial 10 bold")
    label_password.place(x=50, y=150)
    saisie_password = Entry(log_frame, show="*", border=3)
    saisie_password.place(x=150, y=150)


    def checkAuthentification():
        root.destroy()
        robot.principale()


    def inscriptionForm():
        root.destroy()
        inscription.inscription()

    # ------ Bouton Login
    btn_valider = Button(log_frame, text="Login",borderwidth=3, relief='ridge', fg="white", bg=bg_color, width = 15, command=authentification)
    btn_valider.place(x=100, y=200)

    # ----Label et champ du footer
    label_password = Label(log_frame,  text="Vous n'avez pas compte ? ", fg="black", bg="#ffffff", font= "Arial 10 bold")
    label_password.place(x=40, y=320)

    # Sign in
    btn_sign_in = Button(log_frame, text="Sign in",borderwidth=3, relief='ridge', fg="white", bg=bg_color, width = 4, command=inscriptionForm)
    btn_sign_in.place(x=220, y=318)


################################################################################################
#                                DATA BASE CONNEXION
################################################################################################

    root.mainloop()


if __name__=="__main__":
    connexion()
